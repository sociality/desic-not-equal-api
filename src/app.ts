import * as bodyParser from 'body-parser';
import * as express from 'express';
import * as cookieParser from 'cookie-parser';
import { Controller } from './interfaces/lib/controller.interface';
import errorMiddleware from './middleware/error.middleware';
import fs from 'fs';

import 'dotenv/config';
import validateEnv from './utils/validateEnv';

validateEnv();

var path = require('path');
import * as cors from 'cors';

class App {
  public app: express.Application;

  private options: cors.CorsOptions = {
    allowedHeaders: [
      'Access-Control-Allow-Origin',
      'Authorization',
      'Origin',
      'X-Requested-With',
      'Content-Type',
      'Accept',
      'X-Access-Token',
    ],
    credentials: true,
    methods: 'GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE',
    origin: [
      'http://localhost:8080',
      'http://localhost:4200',
      'http://localhost:4300',
      `${process.env.ADMIN_URL}`,
      `${process.env.APP_URL}`,
    ],
    preflightContinue: false,
  };

  constructor(controllers: Controller[]) {
    this.app = express();

    this.initializeMiddlewares();
    this.initializeControllers(controllers);
    this.initializeErrorHandling();
  }

  public listen() {
    this.app.listen(process.env.PORT, () => {
      console.log(`App listening on the port ${process.env.PORT}`);
    });
  }

  public createDirectories(pathname: string) {
    const __dirname = path.resolve();
    pathname = pathname.replace(/^\.*\/|\/?[^\/]+\.[a-z]+|\/$/g, '');

    fs.mkdir(path.resolve(__dirname, pathname), { recursive: true }, (e) => {
      if (e) {
        console.error(e);
      } else {
        console.log(`Folder creating on path ${pathname}`);
      }
    });
  }

  private initializeMiddlewares() {
    this.app.use(bodyParser.json());
    this.app.use('/assets', express.static(path.join(__dirname, '../assets')));
    this.app.use(cookieParser());
    this.app.use(cors(this.options));
  }

  private initializeErrorHandling() {
    this.app.use(errorMiddleware);
  }

  private initializeControllers(controllers: Controller[]) {
    ['../assets/static', '../assets/content'].forEach((folder) => {
      this.createDirectories(path.join(__dirname, folder));
    });

    controllers.forEach((controller) => {
      this.app.use('/', controller.router);
    });
  }
}

export default App;
