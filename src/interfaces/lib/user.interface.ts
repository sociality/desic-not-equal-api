import { AuthInterface, ProfileInterface, EncryptedProfileInterface, DecryptedProfileInterface } from "@interfaces";

export enum UserRole {
  SUPERADMIN = 30,
  ADMIN = 20,
  USER = 10
}

export enum UserCategory {
  LAW = 40,
  MEDICAL = 30,
  SOCIAL = 20,
  MANAGEMENT = 10,
  NONE= 0
}

export interface UserInterface {
  id?: string;

  email: string;
  password: string;

  access: UserRole;
  category: UserCategory;

  communicationId: string;
  refreshTokens?: any;

  auth?: AuthInterface;
  profile?: DecryptedProfileInterface;

  createdAt: Date;
  updatedAt: Date;
}
