import { PostInterface } from "./post.interface";

export interface DecryptedPostInterface extends PostInterface {
  contentFiles: string[];
}
