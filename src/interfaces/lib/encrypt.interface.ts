export interface EncryptInterface {
    iv: string;
    content: string;
}