import { ExaminationInterface, DecryptedUserInterface } from "@interfaces";

export interface DecryptedExaminationInterface extends ExaminationInterface {
  title: string;
  description: string;

  beneficiary: DecryptedUserInterface;
  benefactor: DecryptedUserInterface;
}
