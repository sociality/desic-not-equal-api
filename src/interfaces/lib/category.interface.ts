export enum CategoryType {
    INCIDENT = 'incident',
    SESSION = 'session',
    EXAMINATION = 'examination',
  }
  
export interface CategoryInterface {
    id?: number;
    name: string;
    slug: string;
}