
export interface StatisticsInterface {
  month: string;

  sessions?: number;
  incidents?: number;
  examinations?: number;
}
