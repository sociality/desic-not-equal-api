export interface ConnectionInterface {
    id?: number;

    secret: string;

    createdAt: Date;
    updatedAt: Date;
}