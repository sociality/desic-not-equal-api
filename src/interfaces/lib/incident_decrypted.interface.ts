import { IncidentInterface, DecryptedUserInterface } from "@interfaces";

export interface DecryptedIncidentInterface extends IncidentInterface {
    title: string;
    description: string;

    beneficiary: DecryptedUserInterface;
    benefactor: DecryptedUserInterface;
}
