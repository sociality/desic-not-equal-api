import { UserInterface, EncryptInterface, EncryptedProfileInterface } from "@interfaces";

export interface EncryptedUserInterface extends UserInterface {
  name: EncryptInterface;
  //profile?: EncryptedProfileInterface;
}
