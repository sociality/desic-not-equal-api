import { EncryptInterface } from '@interfaces';
import { EncryptedUserInterface } from './user_encrypted.interface';

export interface ProfileInterface {
  id?: number;
}
