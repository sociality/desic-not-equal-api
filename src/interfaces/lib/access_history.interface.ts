import { EncryptedUserInterface } from "../../interfaces/index";

export enum AccessEditor {
  SUPERADMIN = 'superadmin',
  USER = 'user'
}

export enum AccessCategory {
  LAW = 40,
  MEDICAL = 30,
  SOCIAL = 20,
  ALL = 10
}

export enum AccessAction {
  COMMUNICATE = 30,
  READ = 20,
  WRITE = 10
}

export enum AccessStatus {
  REVOKE = 'revoke',
  NEUTRAL = 'neutral',
  INVOKE = 'invoke',
}

export interface AccessHistoryInterface {
  accessId: number,
  accessAction: AccessAction;
  accessStatus: AccessStatus;
  accessEditor: AccessEditor;
  accessCategory: AccessCategory;

  beneficiaryId: string;

  benefactorId: string;
  name: any;
  access: string;
  category: string;
}
