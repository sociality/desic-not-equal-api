import { IsString, IsEmail } from 'class-validator';

export class RestorationDto {
  @IsString()
  public password: string;

  @IsString()
  public tokenKey: string;
}