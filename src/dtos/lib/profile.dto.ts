import { IsString, ValidateNested } from 'class-validator';
import { AddressInterface } from '@interfaces';
import {AddressDto} from '../index';

export class ProfileDto {
  @ValidateNested()
  public phone: string;

  @ValidateNested()
  public dateOfBirth: string;

  @ValidateNested()
  public maritalStatus: string;

  @ValidateNested()
  public employmentStatus: string;

  @ValidateNested()
  public address: AddressDto;
}
