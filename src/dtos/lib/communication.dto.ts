import { IsString, IsEmail } from 'class-validator';

export class CommunicationDto {
  @IsEmail()
  sender: string;

  @IsString()
  message: string;
}
