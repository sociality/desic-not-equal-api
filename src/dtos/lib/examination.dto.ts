import { IsString, IsBoolean } from 'class-validator';

export class ExaminationDto {
  @IsString()
  public title: string;

  @IsString()
  public description: string;

  @IsString()
  public beneficiaryId: string;

  @IsString()
  public examinationDate: Date;

  @IsString()
  public keywords: string;

  @IsString()
  public category: string;
}