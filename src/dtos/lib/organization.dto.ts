import { AddressDto } from './address.dto';
import { IsString, IsArray, IsOptional, ValidateNested } from 'class-validator';

export class OrganizationDto {
  @IsString()
  public name: string;

  @IsOptional()
  @IsString()
  public image_url: string;

  @IsString()
  public email: string;

  @IsOptional()
  @IsString()
  public phone: string;

  @IsOptional()
  @IsString()
  public phone_2: string;

  @IsOptional()
  @IsString()
  public fax: string;

  @ValidateNested()
  public address: AddressDto;

  @IsString()
  public description: string;

  @IsString()
  public description_2: string;

  @IsOptional()
  @IsArray()
  public social: {
    slug: string;
    value: string;
  }[];
}
