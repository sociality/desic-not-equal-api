import { IsString, IsNumber, IsEmail, IsOptional, ValidateNested } from 'class-validator';
import { ProfileDto } from './profile.dto';

export class UserAuthenticationDto {
  @IsEmail()
  public email: string;

  @IsString()
  public password: string;
}

export class UserRegistrationDto {
  @IsEmail()
  public email: string;

  @IsString()
  public name: string;

  @IsNumber()
  public access: number;

  @IsOptional()
  @IsNumber()
  public category: number;

  @IsOptional()
  @ValidateNested()
  public profile: ProfileDto;
}

export class UpdatePasswordDto {
  @IsString()
  public password: string;

  @IsString()
  public updated: string;
}

export class ForgotPasswordDto {
  @IsEmail()
  public email: string;
}

export class RestorePasswordDto {
  @IsString()
  public password: string;

  @IsString()
  public tokenKey: string;
}
