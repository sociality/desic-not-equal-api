import { Column, Entity, PrimaryGeneratedColumn, ManyToOne, JoinColumn, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { UserEntity } from './user.entity';
import { EncryptInterface } from '@interfaces';

@Entity()
export class IncidentEntity {
    @PrimaryGeneratedColumn('uuid')
    public id?: string;

    @Column({ type: 'json' })
    public title: EncryptInterface;
    // public title: string;

    @Column({ type: 'json' })
    public description: EncryptInterface;
    // public description: string;

    @Column()
    public incidentDate: Date;

    @Column()
    public keywords: string;

    @Column()
    public category: string;

    @ManyToOne(type => UserEntity)
    @JoinColumn()
    beneficiary: UserEntity;

    @Column()
    beneficiaryId: string;

    @ManyToOne(type => UserEntity)
    @JoinColumn()
    benefactor: UserEntity;

    @Column()
    benefactorId: string;

    @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
    public createdAt: Date;

    @UpdateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)", onUpdate: "CURRENT_TIMESTAMP(6)" })
    public updatedAt: Date;
}
