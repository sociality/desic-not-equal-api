import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { CategoryType } from '../../interfaces/index';

@Entity()
export class CategoryEntity {
  @PrimaryGeneratedColumn()
  public id?: number;

  @Column()
  public name: string;

  @Column()
  public slug: string;

  @Column()
  public type: CategoryType;
}
