import { Column, Entity, PrimaryGeneratedColumn, JoinColumn, CreateDateColumn, UpdateDateColumn, OneToMany, OneToOne } from 'typeorm';

import { EncryptInterface } from '@interfaces';
// import { UserEntity } from './user.entity';
import { UserEntity } from './user.entity';

@Entity()
export class UserProfileEntity {
  @PrimaryGeneratedColumn()
  public id?: number;

  @Column('json')
  public address: EncryptInterface;

  @Column('json')
  public phone: EncryptInterface;

  @Column('json')
  public dateOfBirth: EncryptInterface;

  @Column('json')
  public maritalStatus: EncryptInterface;

  @Column('json', { nullable: true })
  public employmentStatus: EncryptInterface;

  @OneToOne(type => UserEntity)
  @JoinColumn()
  user: UserEntity;

  @Column()
  userId: string;
}
