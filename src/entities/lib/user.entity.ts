import { Column, Entity, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, OneToMany, OneToOne, JoinColumn } from 'typeorm';

import { EncryptInterface, UserRole, UserCategory } from '../../interfaces/index';
import { RefreshTokenEntity } from './refreshToken.entity';
import { UserProfileEntity } from './user_profile.entity';
import { UserAuthEntity } from './user_auth.entity';

@Entity()
export class UserEntity {
  @PrimaryGeneratedColumn('uuid')
  public id?: string;

  @Column({ unique: true })
  public email: string;

  @Column()
  public password: string;

  @Column({ type: 'json' })
  public name: EncryptInterface;

  @Column()
  public access: UserRole;

  @Column({ nullable: true })
  public category: UserCategory;

  // @Column({default: 'false'})
  // public verified: boolean;

  // @Column({default: 'true'})
  // public activated: boolean;

  @Column({ nullable: true })
  public communicationId: string;

  // @Column({ nullable: true })
  // public restorationToken?: string;
  //
  // @Column({ nullable: true })
  // public restorationExpiresIn?: string;

  @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
  public createdAt: Date;

  @UpdateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)", onUpdate: "CURRENT_TIMESTAMP(6)" })
  public updatedAt: Date;

  //  @OneToMany(type => RefreshTokenEntity, refreshToken => refreshToken.user)
  //  refreshTokens: RefreshTokenEntity[];

  // @OneToOne(type => UserProfileEntity)//, profile => profile.user)
  // @JoinColumn()
  // profile: UserProfileEntity;

  // @OneToOne(type => UserAuthEntity)//, auth => auth.user)
  // @JoinColumn()
  // auth: UserAuthEntity;
}
