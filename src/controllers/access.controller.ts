import * as express from 'express';
import to from 'await-to-ts';
import path from 'path';
import { getConnection, getManager, getRepository, In } from 'typeorm';

/**
 * DTOs
 */
import { ID, AccessHistoryDto } from '../dtos/index';

/**
 * Entities
 */
import { UserEntity, AccessHistoryEntity } from '../entities/index';

/**
 * Exceptions
 */
import UnprocessableEntityException from '../exceptions/UnprocessableEntity.exception';

/**
 * Interfaces
 */
import { Controller, RequestWithUser } from '../interfaces/index';
import { AccessEditor, AccessHistoryInterface, EncryptedUserInterface, EncryptInterface } from '../interfaces/index';

/**
 * Middleware
 */
import authMiddleware from '../middleware/auth.middleware';

/**
 * Validators
 */
import validationParamsMiddleware from '../validation/params.validation';

/**
 * Utils
 */
import crypto from '../utils/crypto.service';

class AccessController implements Controller {
  public path = '/access';
  public router = express.Router();
  private accessHistoryRepositoy = getRepository(AccessHistoryEntity);
  private userRepositoy = getRepository(UserEntity);

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}/:id`, authMiddleware, validationParamsMiddleware(ID), this.readAccessHistory);
    this.router.put(`${this.path}/:id`, authMiddleware, validationParamsMiddleware(ID), this.updateAccessHistory);
  }

  private readAccessHistory = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const beneficiaryId: ID['id'] = request.params.id;

    let error: Error, usersWithAccess: AccessHistoryInterface[];
    [error, usersWithAccess] = await to(getManager().query(`
        SELECT u.*, u.id as "benefactorId", COALESCE(ac."status", 'invoke') as "accessStatus", COALESCE(ac."id", 0) as "accessId", COALESCE(ac."action", 10) as "accessAction", COALESCE(ac."category", u."category") as "accessCategory"
        FROM user_entity u
        LEFT JOIN access_history_entity ac
		    ON u.id = ac."benefactorId" AND u."access" = 20 AND ac."beneficiaryId" = $1
        WHERE u."access" = 20;
        `, [beneficiaryId]).catch());
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    const result: AccessHistoryInterface[] = usersWithAccess.map((o: AccessHistoryInterface) => {
      return {
        ...o, name: crypto.decrypt(o.name as EncryptInterface)
      }
    });

    response.status(200).send({
      data: { users: result },
      status: true
    });
  }

  private updateAccessHistory = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const data: AccessHistoryDto = request.body;
    const beneficiaryId: ID['id'] = request.params.id;

    let error: Error, result: any;
    [error, result] = await to(this.accessHistoryRepositoy.delete({ beneficiaryId: beneficiaryId, benefactorId: In(data.accesses.map((o: AccessHistoryInterface) => { return o.benefactorId })) }));

    [error, result] = await to(this.accessHistoryRepositoy.save(
      (data.accesses.filter((o: AccessHistoryInterface) => { return o.accessStatus != 'invoke' })).map((o: AccessHistoryInterface) => { return { id: o.accessId, action: o.accessAction, category: o.accessCategory, editor: AccessEditor.USER, status: o.accessStatus, benefactorId: o.benefactorId, beneficiaryId: beneficiaryId } })
    ));

    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    response.status(200).send({
      message: 'OK',
      success: true
    });
  }

  // console.log(data)
  // //const x = (data.users.filter((o: AccessHistoryInterface) => { return o.accessStatus != 'invoke' })).map((o: AccessHistoryInterface) => `(${o.accessId}, ${"'"}${o.accessStatus}${"'"}, ${"'"}user${"'"}, ${"'"}${o.benefactorId}${"'"}, ${"'"}${beneficiaryId}${"'"})`);
  // //console.log(x)
  // // let error: Error, result: any;
  // // [error, result] = await to(getManager().query(`
  // // INSERT INTO access_history_entity (id, status, editor, "benefactorId", "beneficiaryId")
  // // VALUES ($1)
  // //     ON CONFLICT (id) DO UPDATE
  // //     SET status = excluded.status, editor = excluded.editor;
  // // `, []).catch());
  // console.log(data.accesses.map((o: AccessHistoryInterface) => { return o.benefactorId }));

  // let error: Error, result: any;
  // [error, result] = await to(getConnection()
  //     .createQueryBuilder()
  //     .insert()
  //     .into(AccessHistoryEntity)
  //     .values(values)
  //     // .orUpdate({ conflict_target: ['id'], overwrite: ['status'] })
  //     .onConflict(`(id) DO UPDATE SET status = excluded.status, editor = excluded.editor`)
  //     .execute()
  // );

  // private deleteAccessHistory = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
  //   const data: AccessHistoryDto = request.body;
  //   const beneficiaryId = '9119bc58-d3ff-4355-8e4d-967ec524ba37';
  //
  //   console.log((data.accesses.filter((o: AccessHistoryInterface) => { return o.accessStatus === 'invoke' })).map((o: AccessHistoryInterface) => { return o.benefactorId }))
  //   let error: Error, result: any;
  //   [error, result] = await to(getConnection().createQueryBuilder()
  //     .delete()
  //     .from(AccessHistoryEntity)
  //     .where('beneficiaryId = :userId AND benefactorId IN (:...ids)', { userId: beneficiaryId, ids: (data.accesses.filter((o: AccessHistoryInterface) => { return o.accessStatus === 'invoke' })).map((o: AccessHistoryInterface) => { return o.benefactorId }) })
  //     .execute());
  //
  //   next();
  // }



  // private checkAccess(accessHistory: AccessHistoryEntity[], administrator: EncryptedUserInterface) {
  //     const x = (accessHistory).find((b: any) => (b.benefactorId) === (administrator.id));
  //     if (x) return { accessId: x.id, accessType: x.status };
  //     else return { accessId: 0, accessType: 'invoke' };
  // }

  // private readAdministrators = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
  //     let error: Error, administrators: EncryptedUserInterface[];
  //     [error, administrators] = await to(this.userRepositoy.find({
  //         where: { access: 10 },
  //     }));
  //     if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));
  //     console.log(administrators)

  //     response.locals['administrators'] = administrators as EncryptedUserInterface[];
  //     next();
  // }

  // private readAccessHistory = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
  //     const beneficiaryId = '9119bc58-d3ff-4355-8e4d-967ec524ba37';
  //     const administrators = response.locals.administrators;

  //     let error: Error, accessHistory: AccessHistoryEntity[];
  //     [error, accessHistory] = await to(this.accessHistoryRepositoy.find({
  //         where: { beneficiaryId: beneficiaryId, benefactorId: In(administrators.map((o: any) => { return o.id })) }
  //     }));

  //     const access = administrators.map((a: EncryptedUserInterface) =>
  //         Object.assign({}, a,
  //             {
  //                 ...this.checkAccess(accessHistory, a)
  //             }
  //         )
  //     );

  //     response.status(200).send({
  //         data: { administrators: access },
  //         status: true
  //     });
  // }

  private readBeneficiariesByBenefactor = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const benefactorId = '9119bc58-d3ff-4355-8e4d-967ec524ba37';

    let error: Error, users: EncryptedUserInterface[];
    [error, users] = await to(getManager().query(`
        SELECT u.*
        FROM user_entity u
        WHERE u.id NOT IN (
            SELECT ac."beneficiaryId"
            FROM access_history_entity ac
            WHERE ac."benefactorId" = $1
        `, [benefactorId]).catch());
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    response.status(200).send({
      data: { users: users },
      status: true
    });
  }


  // private replaceAt = (access: string, index: number, char: string) => {
  //     var a = access.split("");
  //     a[index] = char;
  //     return a.join("");
  // }

  // private updateAccess = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
  //     const administrators = response.locals.administrators;
  //     const actionType = '1';
  //     const userId: ID['id'] = request.user.id;
  //     const user: EncryptedUserInterface = await this.userRepositoy.findOne(userId);
  //     const privilegesType = 'law';

  //     let error: Error, result: any;
  //     [error, result] = await to(this.userRepositoy.save({

  //     }));
  //     if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

  //     // response.status(200).send({
  //     //     message: `User (with ID: ${profile.id}) has been successfully updated`,
  //     //     success: true
  //     // });

  // }

  // private inClientsHaveAccess = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
  //     const userId = '9f3f2422-a5e3-4d68-9188-c8f44b13c435';

  //     let error: Error, result: AccessHistoryInterface;
  //     [error, result] = await to(getManager().query(`
  //     SELECT *
  //     FROM access_history_entity
  //     WHERE ("access_history_entity"."beneficiaryId", "access_history_entity"."actionAt") IN (
  //             SELECT "access_history_entity"."beneficiaryId", max("access_history_entity"."actionAt")
  //             FROM access_history_entity
  //             GROUP BY "access_history_entity"."beneficiaryId"
  //         ) AND "access_history_entity"."benefactorId" = $1
  //     `, [userId]).catch());
  //     if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

  //     response.status(200).send({
  //         data: { access: result },
  //         status: true
  //     });
  // }

  // private administratorsHaveAccessTo = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
  //     const userId = request.user.id;

  //     let error: Error, result: AccessHistoryInterface;
  //     [error, result] = await to(getManager().query(`
  //     SELECT *
  //     FROM access_history_entity
  //     WHERE ("access_history_entity"."benefactorId", "access_history_entity"."actionAt") IN (
  //             SELECT "access_history_entity"."benefactorId", max("access_history_entity"."actionAt")
  //             FROM access_history_entity
  //             GROUP BY "access_history_entity"."benefactorId"
  //         ) AND "access_history_entity"."beneficiaryId" = $1
  //     `, [userId]).catch());
  //     if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

  //     response.status(200).send({
  //         data: { access: result },
  //         status: true
  //     });
  // }

}

export default AccessController;
