import * as express from 'express';
import to from 'await-to-ts';
import path from 'path';
import { getRepository, getConnection, Not, In } from 'typeorm';

/**
 * Email Service
 */
// import EmailService from '../utils/emailService';
// const emailService = new EmailService();

/**
 * DTOs
 */
import { ID, CategoryDto } from '../dtos/index';

// import CommunicationDto from '../communityDtos/communication.dto'

/**
 * Entities
 */
import { CategoryEntity } from '../entities/index';

/**
 * Exceptions
 */
import UnprocessableEntityException from '../exceptions/UnprocessableEntity.exception';

/**
 * Interfaces
 */
import { Controller, RequestWithUser } from '../interfaces/index';
import { EncryptedIncidentInterface, DecryptedIncidentInterface, CategoryInterface, EncryptedConnectionInterface, DecryptedConnectionInterface, CategoryType } from '@interfaces';

/**
 * Middleware
 */
import validationBodyMiddleware from '../validation/body.validation';
import validationParamsMiddleware from '../validation/params.validation';
import authMiddleware from '../middleware/auth.middleware';
import accessMiddleware from '../middleware/access.middleware';

/**
 * Utils
 */
import crypto from '../utils/crypto.service';

class CategoryController implements Controller {
  public path = '/categories';
  public router = express.Router();
  private categoryRepository = getRepository(CategoryEntity);

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}/:type`, /*authMiddleware,*/ this.readCategories);
    this.router.put(`${this.path}/:type`, /*authMiddleware,*/ /*validationBodyMiddleware(CategoryDto),*/ this.updateCategories);
  }

  private readCategories = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const type = request.params.type as CategoryType;

    let error: Error, categories: CategoryInterface[];
    [error, categories] = await to(this.categoryRepository.find({ type: type }));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    response.status(200).send({
      data: { categories: categories },
      status: true
    });
  }

  private updateCategories = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const data: CategoryDto = request.body;
    const type = request.params.type as CategoryType;

    let error: Error, result: any;
    // [error, result] = await to(this.categoryRepository.delete({ id: Not(In(data.categories.map(a => a.id))), type: type }));

    [error, result] = await to(this.categoryRepository.save(
      (data.categories).map((o: CategoryInterface) => { return { id: o.id, name: o.name, slug: o.slug, type: type } })
    ));
    [error, result] = await to(this.categoryRepository.delete({ id: Not(In(data.categories.map((a: CategoryInterface) => a.id))), type: type }));

    // [error, result] = await to(getConnection().createQueryBuilder()
    //   .insert()
    //   .into(CategoryEntity)
    //   .values((data.categories).map((o: CategoryInterface) => { return { id: o.id, name: o.name, slug: o.slug, type: type } }))
    //   // .onConflict(`("id") DO UPDATE SET "name" = excluded."name", "slug" = excluded."slug"`)
    //   .onConflict({ conflict_target: ['id'], overwrite: ['name', 'slug', 'type'] })
    //   .execute());

    // [error, result] = await to(getConnection()
    //   .createQueryBuilder()
    //   .insert()
    //   .into(CategoryEntity)
    //   .values((data.categories).map((o: CategoryInterface) => { return { id: o.id, name: o.name, slug: o.slug, type: type } }))
    //   .orUpdate({ conflict_target: ['id'], overwrite: ['name', 'slug', 'type'] })
    //   .execute()
    // );
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    response.status(200).send({
      message: `Categories (of type: ${type}) has been successfully updated`,
      success: true
    });
  }

}

export default CategoryController;
