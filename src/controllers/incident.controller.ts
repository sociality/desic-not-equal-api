import * as express from 'express';
import to from 'await-to-ts';
import path from 'path';
import { getRepository, In, Not } from 'typeorm';

/**
 * Email Service
 */
// import EmailService from '../utils/emailService';
// const emailService = new EmailService();

/**
 * DTOs
 */
import { ID, IncidentDto } from '../dtos/index';

// import CommunicationDto from '../communityDtos/communication.dto'

/**
 * Entities
 */
import { IncidentEntity } from '../entities/index';

/**
 * Exceptions
 */
import UnprocessableEntityException from '../exceptions/UnprocessableEntity.exception';

/**
 * Interfaces
 */
import { Controller, RequestWithUser } from '../interfaces/index';
import { EncryptedIncidentInterface, DecryptedIncidentInterface, EncryptedUserInterface, AccessAction, AccessCategory } from '../interfaces/index';

/**
 * Middleware
 */
import validationBodyMiddleware from '../validation/body.validation';
import validationParamsMiddleware from '../validation/params.validation';
import authMiddleware from '../middleware/auth.middleware';
import accessMiddleware from '../middleware/access.middleware';

/**
 * Utils
 */
import crypto from '../utils/crypto.service';

class IncidentController implements Controller {
  public path = '/incidents';
  public router = express.Router();
  private incidentRepositoy = getRepository(IncidentEntity);

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}/`, authMiddleware, this.declareAccessCategoryAndAction, accessMiddleware.clientsCannotAccess, this.readIncidents);
    this.router.get(`${this.path}/client`, authMiddleware, this.readIncidentsByClient);
    this.router.get(`${this.path}/:id`, authMiddleware, validationParamsMiddleware(ID), this.readIncidentById);
    this.router.post(`${this.path}/`, authMiddleware, validationBodyMiddleware(IncidentDto), this.createIncident);
    this.router.put(`${this.path}/:id`, authMiddleware, validationParamsMiddleware(ID), validationBodyMiddleware(IncidentDto), this.updateIncident);
    this.router.delete(`${this.path}/:id`, authMiddleware, validationParamsMiddleware(ID), this.deleteIncident);
  }

  private declareAccessCategoryAndAction = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    response.locals = {
      action: AccessAction.READ,
      category: AccessCategory.LAW,
    }

    next();
  }

  private readIncidents = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const accessRevoke: string[] = response.locals.access; // Access

    let error: Error, incidents: EncryptedIncidentInterface[];
    [error, incidents] = await to(this.incidentRepositoy.find(
      {
        relations: ["benefactor", "beneficiary"],
        where: {
          beneficiaryId: Not(In(accessRevoke)) // Access
        }
      }));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    const result: DecryptedIncidentInterface[] = incidents.map((o: EncryptedIncidentInterface) => {
      return {
        ...o,
        title: crypto.decrypt(o.title), description: crypto.decrypt(o.description),
        benefactor: { ...o.benefactor, name: crypto.decrypt(o.benefactor.name) },
        beneficiary: { ...o.beneficiary, name: crypto.decrypt(o.beneficiary.name) }
      }
    });

    response.status(200).send({
      data: { incidents: result },
      status: true
    });
  }

  private readIncidentById = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const incidentId: ID['id'] = request.params.id;

    let error: Error, incident: EncryptedIncidentInterface;
    [error, incident] = await to(this.incidentRepositoy.findOne({ id: incidentId }, { relations: ["benefactor", "beneficiary"] }));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    const result: DecryptedIncidentInterface = {
      ...incident,
      title: crypto.decrypt(incident.title), description: crypto.decrypt(incident.description),
      benefactor: { ...incident.benefactor, name: crypto.decrypt(incident.benefactor.name) },
      beneficiary: { ...incident.beneficiary, name: crypto.decrypt(incident.beneficiary.name) }
    };

    response.status(200).send({
      data: { incident: result },
      status: true
    });
  }

  private readIncidentsByClient = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const userId: string = request.user.id;

    let error: Error, incidents: EncryptedIncidentInterface[];
    [error, incidents] = await to(this.incidentRepositoy.find({
      relations: ["benefactor", "beneficiary"],
      where: { beneficiaryId: userId }
    }));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    const result: DecryptedIncidentInterface[] = incidents.map((o: EncryptedIncidentInterface) => {
      return {
        ...o,
        title: crypto.decrypt(o.title), description: crypto.decrypt(o.description),
        benefactor: { ...o.benefactor, name: crypto.decrypt(o.benefactor.name) },
        beneficiary: { ...o.beneficiary, name: crypto.decrypt(o.beneficiary.name) }
      }
    });

    response.status(200).send({
      data: { incidents: result },
      status: true
    });
  }

  private createIncident = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const data: IncidentDto = request.body;
    const user: EncryptedUserInterface = request.user;

    const incident = new IncidentEntity();
    incident.title = crypto.encrypt(data.title);
    incident.description = crypto.encrypt(data.description);
    incident.incidentDate = data.incidentDate;
    incident.keywords = data.keywords;
    incident.category = data.category;
    incident.beneficiaryId = data.beneficiaryId;

    incident.benefactorId = user.id;

    let error: Error, result: any;
    const newIncident = this.incidentRepositoy.create(incident);
    [error, result] = await to(this.incidentRepositoy.save(newIncident));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    response.status(200).send({
      message: `A new Incident (with ID: ${result.id}) has been successfully created`,
      success: true
    });
  }

  private updateIncident = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const data: IncidentDto = request.body;
    const user: EncryptedUserInterface = request.user;

    const incidentId: ID['id'] = request.params.id;
    const incident = await this.incidentRepositoy.findOne(incidentId);

    let error: Error, result: any;
    [error, result] = await to(this.incidentRepositoy.save({
      ...incident,
      title: crypto.encrypt(data.title),
      description: crypto.encrypt(data.description),
      incidentDate: data.incidentDate,
      keywords: data.keywords,
      category: data.category,
      benefactorId: user.id
    }));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    response.status(200).send({
      message: `Incident (with ID: ${incident.id}) has been successfully updated`,
      success: true
    });
  }

  private deleteIncident = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const incidentId: ID['id'] = request.params.id;

    let error: Error, result: any;
    [error, result] = await to(this.incidentRepositoy.delete(incidentId));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    response.status(200).send({
      message: `Incident (with ID: ${incidentId}) has been successfully deleted`,
      success: true
    });
  }
}

export default IncidentController;
