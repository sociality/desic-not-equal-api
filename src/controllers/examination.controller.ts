import * as express from 'express';
import to from 'await-to-ts';
import path from 'path';
import { getRepository, In, Not } from 'typeorm';

/**
 * Email Service
 */
// import EmailService from '../utils/emailService';
// const emailService = new EmailService();

/**
 * DTOs
 */
import { ID, ExaminationDto } from '../dtos/index';

// import CommunicationDto from '../communityDtos/communication.dto'

/**
 * Entities
 */
import { ExaminationEntity } from '../entities/index';

/**
 * Exceptions
 */
import UnprocessableEntityException from '../exceptions/UnprocessableEntity.exception';

/**
 * Interfaces
 */
import { Controller, RequestWithUser } from '../interfaces/index';
import { EncryptedExaminationInterface, DecryptedExaminationInterface, EncryptedUserInterface, AccessAction, AccessCategory } from '../interfaces/index';

/**
 * Middleware
 */
import validationBodyMiddleware from '../validation/body.validation';
import validationParamsMiddleware from '../validation/params.validation';
import authMiddleware from '../middleware/auth.middleware';
import accessMiddleware from '../middleware/access.middleware';

/**
 * Utils
 */
import crypto from '../utils/crypto.service';

class ExaminationController implements Controller {
  public path = '/examinations';
  public router = express.Router();
  private examinationRepositoy = getRepository(ExaminationEntity);

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}/`, authMiddleware, this.declareAccessCategoryAndAction, accessMiddleware.clientsCannotAccess, this.readExaminations);
    this.router.get(`${this.path}/client`, authMiddleware, this.readExaminationsByClient);
    this.router.get(`${this.path}/:id`, authMiddleware, validationParamsMiddleware(ID), this.readExaminationById);
    this.router.post(`${this.path}/`, authMiddleware, validationBodyMiddleware(ExaminationDto), this.createExamination);
    this.router.put(`${this.path}/:id`, authMiddleware, validationParamsMiddleware(ID), validationBodyMiddleware(ExaminationDto), this.updateExamination);
    this.router.delete(`${this.path}/:id`, authMiddleware, validationParamsMiddleware(ID), this.deleteExamination);
  }

  private declareAccessCategoryAndAction = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    response.locals = {
      action: AccessAction.READ,
      category: AccessCategory.MEDICAL,
    }

    next();
  }

  private readExaminations = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const accessRevoke: string[] = response.locals.access; // Access

    let error: Error, examinations: EncryptedExaminationInterface[];
    [error, examinations] = await to(this.examinationRepositoy.find({
      relations: ["benefactor", "beneficiary"],
      where: {
        beneficiaryId: Not(In(accessRevoke)) // Access
      }
    }));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    const result: DecryptedExaminationInterface[] = examinations.map((o: EncryptedExaminationInterface) => {
      return {
        ...o,
        title: crypto.decrypt(o.title), description: crypto.decrypt(o.description),
        benefactor: { ...o.benefactor, name: crypto.decrypt(o.benefactor.name) },
        beneficiary: { ...o.beneficiary, name: crypto.decrypt(o.beneficiary.name) }
      }
    });

    response.status(200).send({
      data: { examinations: result },
      status: true
    });
  }

  private readExaminationById = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const examinationId: ID['id'] = request.params.id;

    let error: Error, examination: EncryptedExaminationInterface;
    [error, examination] = await to(this.examinationRepositoy.findOne({ id: examinationId }, { relations: ["benefactor", "beneficiary"] }));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    const result: DecryptedExaminationInterface = {
      ...examination,
      title: crypto.decrypt(examination.title), description: crypto.decrypt(examination.description),
      benefactor: { ...examination.benefactor, name: crypto.decrypt(examination.benefactor.name) },
      beneficiary: { ...examination.beneficiary, name: crypto.decrypt(examination.beneficiary.name) }
    };

    response.status(200).send({
      data: { examination: result },
      status: true
    });
  }

  private readExaminationsByClient = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const userId: string = request.user.id;

    let error: Error, examinations: EncryptedExaminationInterface[];
    [error, examinations] = await to(this.examinationRepositoy.find({
      relations: ["benefactor", "beneficiary"],
      where: { beneficiaryId: userId }
    }));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    const result: DecryptedExaminationInterface[] = examinations.map((o: EncryptedExaminationInterface) => {
      return {
        ...o,
        title: crypto.decrypt(o.title), description: crypto.decrypt(o.description),
        benefactor: { ...o.benefactor, name: crypto.decrypt(o.benefactor.name) },
        beneficiary: { ...o.beneficiary, name: crypto.decrypt(o.beneficiary.name) }
      }
    });

    response.status(200).send({
      data: { examinations: result },
      status: true
    });
  }

  private createExamination = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const data: ExaminationDto = request.body;
    const user: EncryptedUserInterface = request.user;

    const examination = new ExaminationEntity();
    examination.title = crypto.encrypt(data.title);
    examination.description = crypto.encrypt(data.description);
    examination.examinationDate = data.examinationDate;
    examination.keywords = data.keywords;
    examination.category = data.category;
    examination.beneficiaryId = data.beneficiaryId;
    examination.benefactorId = user.id;

    let error: Error, result: any;
    const newExamination = this.examinationRepositoy.create(examination);
    [error, result] = await to(this.examinationRepositoy.save(newExamination));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    response.status(200).send({
      message: `A new Examination (with ID: ${result.id}) has been successfully created`,
      success: true
    });
  }

  private updateExamination = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const data: ExaminationDto = request.body;
    const user: EncryptedUserInterface = request.user;

    const examinationId: ID['id'] = request.params.id;
    const examination = await this.examinationRepositoy.findOne(examinationId);

    let error: Error, result: any;
    [error, result] = await to(this.examinationRepositoy.save({
      ...examination,
      title: crypto.encrypt(data.title),
      description: crypto.encrypt(data.description),
      benefactorId: user.id
    }));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    response.status(200).send({
      message: `Examination (with ID: ${examination.id}) has been successfully updated`,
      success: true
    });
  }

  private deleteExamination = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const examinationId: ID['id'] = request.params.id;

    let error: Error, result: any;
    [error, result] = await to(this.examinationRepositoy.delete(examinationId));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    response.status(200).send({
      message: `Examination (with ID: ${examinationId}) has been successfully deleted`,
      success: true
    });
  }
}

export default ExaminationController;
