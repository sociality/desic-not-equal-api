import { NextFunction, Response } from 'express';
import * as nodemailer from 'nodemailer';
import to from 'await-to-ts';
import Promise, { props } from 'bluebird';

import 'dotenv/config';
import validateEnv from './validateEnv';

validateEnv();

import Transporter from '../utils/transporter.service';
const Email = require('email-templates');
const email = new Email();

import { UserRole, RequestWithUser } from '../interfaces/index';

class EmailService {

  private emailSender = (options: { type: any; locals: any; from: any; to: any; subject: any; }) => {
    return Promise.all([email.render(options.type, options.locals)])
      .then((template: object) => {
        const mailOptions: nodemailer.SendMailOptions = {
          from: options.from,
          to: (`${process.env.PRODUCTION}` == 'true') ? options.from : `${process.env.EMAIL_TEST}`,
          subject: options.subject,
          html: template.toString()
        };
        return Transporter.sendMail(mailOptions);
      }).catch(error => { console.log("ERROR", error) });
  }

  public clientCommunication = async (request: RequestWithUser, response: Response, next: NextFunction) => {
    const data = response.locals;

    let options = {
      from: process.env.EMAIL_FROM,
      to: data.organization.email,
      subject: 'You have a new message from a member',
      html: '',
      type: 'communication',
      locals: {
        logo_url: `${process.env.LOGO_URL}`,
        home_page: `${process.env.ADMIN_URL}`,
        sender: data.sender,
        message: data.message
      },
    }

    let error, results: object = {};
    [error, results] = await to(this.emailSender(options));
    if (error) {
      return response.status(422).send({
        message: `Email ERROR || ${error}`,
        success: false
      });
    }

    response.status(200).send({
      message: "Your message has been sent",
      success: true
    });
  }

  public passwordRestoration = async (request: RequestWithUser, response: Response, next: NextFunction) => {
    const data = response.locals;

    const origin = (data.access == UserRole.USER) ? `${process.env.APP_URL}` : `${process.env.ADMIN_URL}`;

    let options = {
      from: process.env.EMAIL_FROM,
      to: data.email,
      subject: 'Reset your DeSIC Account password',
      html: '',
      type: 'restoration',
      locals: {
        logo_url: `${process.env.LOGO_URL}`,
        home_page: `${origin}`,
        link: `${origin}/auth/reset-password?tokenKey=${data.token}`
      },
    }

    console.log(options);
    let error, results: object = {};
    [error, results] = await to(this.emailSender(options));
    if (error) {
      console.log(error)
      return response.status(422).send({
        message: `Email ERROR || ${error}`,
        success: false
      });
    }

    response.status(200).send({
      message: "A email has been sent to you account",
      success: true
    });
  }

  public userRegistration = async (request: RequestWithUser, response: Response, next: NextFunction) => {
    const data = response.locals;

    const origin = (data.access == UserRole.USER) ? `${process.env.APP_URL}` : `${process.env.ADMIN_URL}`;

    let options = {
      from: process.env.EMAIL_FROM,
      to: data.email,
      subject: 'Welcome to DeSIC Network',
      html: '',
      type: 'registration',
      locals: {
        logo_url: `${process.env.LOGO_URL}`,
        home_page: `${origin}`,
        link: `${origin}/auth/login`,
        password: data.password
      },
    }

    let error, results: object = {};
    [error, results] = await to(this.emailSender(options));
    if (error) {
      return response.status(422).send({
        message: `Email ERROR || ${error}`,
        success: false
      });
    }

    response.status(200).send({
      message: "A new User (with email: " + data.email + ") has been successfully created",
      success: true
    });
  }

  public userDeletion = async (request: RequestWithUser, response: Response, next: NextFunction) => {
    const data = response.locals;

    let options = {
      from: process.env.EMAIL_FROM,
      to: data.email,
      subject: 'Goodbye from DeSIC Network',
      html: '',
      type: 'deletion',
      locals: {
        logo_url: `${process.env.LOGO_URL}`,
        home_page: `${process.env.APP_URL}`,
      },
    }

    let error, results: object = {};
    [error, results] = await to(this.emailSender(options));
    if (error) {
      return response.status(422).send({
        message: `Email ERROR || ${error}`,
        success: false
      });
    }

    response.status(200).send({
      message: "User (with ID: " + data.id + ") has been successfully deleted",
      success: true
    });
  }
}
export default EmailService;
